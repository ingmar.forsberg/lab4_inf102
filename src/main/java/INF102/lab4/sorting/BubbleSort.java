package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        boolean swapped = true;
        while(swapped) {
            swapped = false;
            for(int i = 0; i < list.size() - 1; i++ ) {
                if(list.get(i).compareTo(list.get(i+1)) > 0) {
                    swap(list, i, i+1);
                    swapped = true;
                }

                
            }
        }
        
    }

    private static <T> void swap(List<T> list, int a, int b) {
        T temp = list.get(a);
        list.set(a, list.get(b));
        list.set(b, temp);
    }

    
    
}
