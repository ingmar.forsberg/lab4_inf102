package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        return quickSelect(listCopy, 0, list.size() -  1, list.size() / 2);
    }

    private static <T extends Comparable<T>> T quickSelect(List<T> list, int left, int right, int k) { 
        if(left >= right) {
            return list.get(left);
        }
        int pivotIndex = partition(list, left, right);
        if (k == pivotIndex) {
            return list.get(k);
        } else if (k < pivotIndex) {
            return quickSelect(list, left, pivotIndex - 1, k);
        } else {
            return quickSelect(list, pivotIndex + 1, right, k);
        }
        
    }

    private static <T extends Comparable<T>> int partition(List<T> list, int start, int end) {
        int randomIndex = new Random().nextInt((end - start)) + start;
        swap(list, randomIndex, end);
       
        T pivot = list.get(end);
        int i = start;

       for(int j = start; j <= end - 1; j++) {
        if(list.get(j).compareTo(pivot) < 0) {
            swap(list, i, j);
            i++;
        }
       }
       swap(list, i, end);
       return i;

    }

    private static <T> void swap(List<T> list, int a, int b) {
        T temp = list.get(a);
        list.set(a, list.get(b));
        list.set(b, temp);
    }
    
}




